FROM ubuntu:18.04

WORKDIR /app

RUN apt-get update \
    && apt-get install -y \
    openmpi-doc openmpi-bin libopenmpi-dev \
    openssh-client g++ libboost-all-dev \
    && useradd mpiuser
