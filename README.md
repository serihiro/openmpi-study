# MPI Study code flagments

# Setup

```sh
make build
make run
```

# compile

```sh
make run
mpicc hello.c -o hello
mpicxx -I/boost/include -L/boost/lib quad_sample.cxx -o quad -lboost_mpi -lboost_serialization -Wall
```

# run

```sh
mpirun -np 4 hello
```
