#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>

int main(int argc, char **argv)
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size == 1)
    {
        printf("This example required more than one process to execute\n");
        MPI_Finalize();
        exit(1);
    }

    int messages[2];
    MPI_Status status;

    if (rank != 0)
    {
        messages[0] = rank;
        messages[1] = size;
        int dest = 0;
        int tag = 0;
        MPI_Send(messages, 2, MPI_INT, dest, tag, MPI_COMM_WORLD);
    }
    else
    {
        for (int src = 1; src < size; ++src)
        {
            MPI_Recv(messages, 2, MPI_INT, src, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            printf("Hello from process %d of %d\n", messages[0], messages[1]);
        }
    }

    MPI_Finalize();
    return 0;
}
