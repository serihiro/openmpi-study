#include <iostream>
#include <array>
#include <string>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

namespace bmpi = boost::mpi;

void initialize_weight(std::array<float, 5> *weight);
std::array<float, 100> affine(std::array<std::array<float, 5>, 100> weight, std::array<float, 5> input);
std::array<float, 25> slice_array(std::array<float, 100> src, int begin, int end);

// 疑似Affineコード
// $ mpicxx -I/boost/include -L/boost/lib quad_sample.cxx -o quad -lboost_mpi -lboost_serialization -Wall
// $ mpirun -np 4 quad
int main(int argc, char **argv)
{
    bmpi::environment env(argc, argv);
    bmpi::communicator world;

    std::array<float, 5> input_0 = {0.1, 0.1, 0.1, 0.1, 0.1};
    std::array<std::array<float, 5>, 100> weight_0 = {};
    for (auto itr = weight_0.begin(); itr != weight_0.end(); ++itr)
    {
        initialize_weight(itr);
    }

    std::array<float, 5> input_1 = {0.2, 0.2, 0.2, 0.2, 0.2};
    std::array<std::array<float, 5>, 100> weight_1 = {};
    for (auto itr = weight_1.begin(); itr != weight_1.end(); ++itr)
    {
        initialize_weight(itr);
    }

    std::array<float, 5> input_2 = {0.3, 0.3, 0.3, 0.3, 0.3};
    std::array<std::array<float, 5>, 100> weight_2 = {};
    for (auto itr = weight_2.begin(); itr != weight_2.end(); ++itr)
    {
        initialize_weight(itr);
    }

    std::array<float, 5> input_3 = {0.4, 0.4, 0.4, 0.4, 0.4};
    std::array<std::array<float, 5>, 100> weight_3 = {};
    for (auto itr = weight_3.begin(); itr != weight_3.end(); ++itr)
    {
        initialize_weight(itr);
    }

    std::array<float, 25> output;
    std::array<float, 100> local_result;
    switch (world.rank())
    {
    case 0:
        local_result = affine(weight_0, input_0);
        for (int i = 0; i < 25; ++i)
        {
            output[i] = local_result[i];
        }
        break;
    case 1:
        local_result = affine(weight_1, input_1);
        for (int i = 25; i < 50; ++i)
        {
            output[i - 25] = local_result[i];
        }
        break;
    case 2:
        local_result = affine(weight_2, input_2);
        for (int i = 50; i < 75; ++i)
        {
            output[i - 50] = local_result[i];
        }
        break;
    case 3:
        local_result = affine(weight_3, input_3);
        for (int i = 75; i < 100; ++i)
        {
            output[i - 75] = local_result[i];
        }
    }

    world.barrier();
    std::cout << local_result[0] << ","
              << local_result[1] << ","
              << local_result[2] << ","
              << local_result[3] << ","
              << local_result[4] << ","
              << local_result[5] << ","
              << local_result[6] << ","
              << local_result[7] << ","
              << local_result[8] << ","
              << local_result[9] << ","
              << std::endl;

    // 0 -> other node
    if (world.rank() == 0)
    {
        world.send(1, 0, slice_array(local_result, 25, 50));
        world.send(2, 0, slice_array(local_result, 50, 75));
        world.send(3, 0, slice_array(local_result, 75, 100));
    }
    else
    {
        std::array<float, 25> value;
        world.recv(0, 0, value);
        for (int i = 0; i < 25; ++i)
        {
            output[i] += value[i];
        }
    }

    // 1 -> other node
    if (world.rank() == 1)
    {
        world.send(0, 0, slice_array(local_result, 0, 25));
        world.send(2, 0, slice_array(local_result, 50, 75));
        world.send(3, 0, slice_array(local_result, 75, 100));
    }
    else
    {
        std::array<float, 25> value;
        world.recv(1, 0, value);
        for (int i = 0; i < 25; ++i)
        {
            output[i] += value[i];
        }
    }

    // 2 -> other node
    if (world.rank() == 2)
    {
        world.send(0, 0, slice_array(local_result, 0, 25));
        world.send(1, 0, slice_array(local_result, 25, 50));
        world.send(3, 0, slice_array(local_result, 75, 100));
    }
    else
    {
        std::array<float, 25> value;
        world.recv(2, 0, value);
        for (int i = 0; i < 25; ++i)
        {
            output[i] += value[i];
        }
    }

    // 3 -> other node
    if (world.rank() == 3)
    {
        world.send(0, 0, slice_array(local_result, 0, 25));
        world.send(1, 0, slice_array(local_result, 25, 50));
        world.send(2, 0, slice_array(local_result, 50, 75));
    }
    else
    {
        std::array<float, 25> value;
        world.recv(3, 0, value);
        for (int i = 0; i < 25; ++i)
        {
            output[i] += value[i];
        }
    }

    world.barrier();

    std::cout << "done" << world.rank() << std::endl;
    // done0
    // result:0:5
    // done2
    // result:2:5
    // done3
    // result:3:5
    // done1
    // result:1:5
    std::cout << "result:" << world.rank() << ":" << output[0] << std::endl;

    return 0;
}

void initialize_weight(std::array<float, 5> *weight)
{
    *weight = {1.0, 1.0, 1.0, 1.0, 1.0};
}

std::array<float, 100> affine(std::array<std::array<float, 5>, 100> weight, std::array<float, 5> input)
{
    std::array<float, 100> local_result = {};
    for (int i = 0; i < 100; ++i)
    {
        for (int j = 0; j < 5; ++j)
        {
            local_result[i] += weight[i][j] * input[j];
        }
    }

    return local_result;
}

std::array<float, 25> slice_array(std::array<float, 100> src, int begin, int end)
{
    std::array<float, 25> dst = {};
    std::copy(src.begin() + begin, src.begin() + end, dst.begin());

    return dst;
}
