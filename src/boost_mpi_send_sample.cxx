#include <boost/mpi/communicator.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/nonblocking.hpp>
#include <iostream>
#include <ctime>
#include <cstdlib>

namespace bmpi = boost::mpi;

int main(int argc, char **argv)
{
    bmpi::environment env(argc, argv);
    bmpi::communicator world;

    std::srand(std::time(0));

    if (world.rank() % 2 == 0)
    {
        const int target_node = world.rank() + 1;
        double value = double(std::rand()) / RAND_MAX;
        world.send(target_node, 0, value);
        std::cout << "send : " << value << std::endl;
    }
    else
    {
        const int target_node = world.rank() - 1;
        double value;
        world.recv(target_node, 0, value);
        std::cout << "recv : " << value << std::endl;
    }
}
