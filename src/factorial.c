#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
    int num_procs;
    int my_proc;
    int i, n, result, result_local, result_recv, result_send;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_proc);

    if (num_procs != 2)
    {
        printf("number of processes must be 2.\n");
        MPI_Finalize();
        return 0;
    }

    switch (my_proc)
    {
    case 0:
        n = 1;
        break;
    case 1:
        n = 51;
        break;
    }

    result_local = 0;
    for (i = n; i < n + 50; i++)
        result_local += i;

    result_send = result_local;

    if (my_proc == 0)
        MPI_Recv(&result_recv, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);
    else
        MPI_Send(&result_send, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

    if (my_proc == 0)
    {
        result = result_local + result_recv;
        printf("result = %d\n", result);
    }

    MPI_Finalize();
}
